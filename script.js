let arrayBtnId = [];
let newArrayBtnId = [];

let btn = document.querySelectorAll(".btn");
let btn2 = document.querySelectorAll(".btn").forEach(function (btn) {
  let buttonID = btn.getAttribute("id").toUpperCase();
  arrayBtnId.push(buttonID);
});

for (el of arrayBtnId) {
  if (el.length > 1) {
    let newEl = el.charAt(0).toUpperCase() + el.slice(1).toLowerCase();
    newArrayBtnId.push(newEl);
  } else {
    newArrayBtnId.push(el);
  }
}

document.addEventListener("keyup", function (event) {
  newArrayBtnId.forEach((name) => {
    if (event.code === `Key${name}`) {
      btn.forEach((nameid) => {
        if (nameid.getAttribute("id").toUpperCase() === name) {
          nameid.classList.add("blue");
        } else {
          nameid.classList.remove("blue");
        }
      });
    } else if (event.code === "Enter" && name === "Enter") {
      btn.forEach((nameid) => {
        if (
          nameid.getAttribute("id").toUpperCase() === name ||
          nameid.getAttribute("id").toLowerCase() === "enter"
        ) {
          nameid.classList.add("blue");
        } else {
          nameid.classList.remove("blue");
        }
      });
    }
  });
});
